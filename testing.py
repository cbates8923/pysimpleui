import button
import pygame

pygame.font.init()

WIDTH = 1000
HEIGHT = 800
WIN = pygame.display.set_mode((WIDTH, HEIGHT))


def main():
    run = True
    clock = pygame.time.Clock()
    button_obj = button.Button(
        "minimalistic-dark", "Hello!", 100, 100, 200, 100, 3)

    while run:
        clock.tick(60)

        events = pygame.event.get()

        for event in events:
            if event.type == pygame.QUIT:
                run = False

        WIN.fill((255, 255, 255))

        button_obj.run(events, pygame.mouse, WIN)

        pygame.display.update()


main()
