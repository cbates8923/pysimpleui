def get_var_or_none(dictionary, key):
    try:
        return dictionary[key]
    except:
        return None


def linearly_set_color(color_increased, color_increased_to):
    color_increased = list(color_increased)
    color_increased_to = list(color_increased_to)
    for i in range(3):
        if color_increased[i] < color_increased_to[i]:
            color_increased[i] += 1
        elif color_increased[i] > color_increased_to[i]:
            color_increased[i] -= 1

    return tuple(color_increased)


def linearly_set_int(int_increased, int_increased_to):
    if int_increased < int_increased_to:
        return int_increased + 1
    return int_increased - 1


def conv_alignment_to_num(alignment):
    if alignment == "center":
        return 0
    if alignment == "left":
        return -50
    if alignment == "right":
        return 50
