import pygame
import json
from global_funcs import *


styles_file = open("styles.json", "r").read()

STYLES = json.loads(styles_file)


class Button:
    def __init__(self, button_style, text, x, y, width, height, border_width):
        # Variables for drawing button
        self.border_width = border_width
        self.text = text
        self.x = x
        self.y = y
        self.width = width
        self.height = height

        # Open style dictionary for specified animation
        self.style_dict = STYLES["button"][button_style]

        # set color now bc ya
        self.color = get_var_or_none(self.style_dict, "color")

        # Progress and anim running bools
        self.anim_clicking = False
        self.anim_clicking_color_prog = (0, 0, 0)
        self.anim_clicking_border_rad_prog = 0
        self.anim_clicking_border_color_prog = (0, 0, 0)
        self.anim_clicking_text_color_prog = (0, 0, 0)
        self.anim_clicking_text_align_prog = 0
        self.anim_hover = False
        self.anim_hover_color_prog = (0, 0, 0)
        self.anim_hover_border_rad_prog = 0
        self.anim_hover_border_color_prog = (0, 0, 0)
        self.anim_hover_text_color_prog = (0, 0, 0)
        self.anim_hover_text_align_prog = 0

        # Other variables
        self.font = pygame.font.SysFont(self.style_dict["text-font"], 40)

        # Animation current variables
        self.click_set1, self.click_set2, self.click_set3, self.click_set4, self.click_set5 = True, True, True, True, True
        self.hover_set1, self.hover_set2, self.hover_set3, self.hover_set4, self.hover_set5 = True, True, True, True, True

    def run(self, events, mouse, win):
        # Draw button & check for hover and clicks
        self.draw(win)
        self.move(events, mouse)

    def draw(self, win):
        """This handles drawing all of the objects onto the screen 
        but not handling animations."""

        # The animation for clicking
        if self.anim_clicking:
            # Draw button body
            pygame.draw.rect(win, self.anim_clicking_color_prog, (self.x, self.y, self.width,
                             self.height), 0, self.anim_clicking_border_rad_prog)

            # Draw button border
            pygame.draw.rect(win, self.anim_clicking_border_color_prog, (self.x, self.y, self.width,
                             self.height), self.border_width, self.anim_clicking_border_rad_prog)

            # Render text with alignment
            text = self.font.render(
                self.text, True, self.anim_clicking_text_color_prog)
            text_align = get_var_or_none(self.style_dict, "text-align-clicked")
            text_x, text_y = self.get_text_pos_with_alignment(text, text_align)

            # blit text to screen.
            win.blit(text, (text_x, text_y))

        # This is very similar to what's above but for hover animations.
        elif self.anim_hover:
            # Draw button body
            pygame.draw.rect(win, self.anim_hover_color_prog, (self.x, self.y, self.width,
                             self.height), 0, self.anim_hover_border_rad_prog)

            # Draw button border
            pygame.draw.rect(win, self.anim_hover_border_color_prog, (self.x, self.y, self.width,
                             self.height), self.border_width, self.anim_hover_border_rad_prog)

            # Render text with alignment
            text = self.font.render(
                self.text, True, self.anim_hover_text_color_prog)
            text_align = get_var_or_none(self.style_dict, "text-align-hover")
            text_x, text_y = self.get_text_pos_with_alignment(text, text_align)

            # blit text to screen.
            win.blit(text, (text_x, text_y))

        # This is also very similar but using the hardcoded values
        # from the button's style
        else:
            # Get the dict var for the button's border radius
            border_rad = get_var_or_none(self.style_dict, "border-radius")

            # If the dict has not border-radius value then set to -1
            # (no border radius)
            if not border_rad:
                border_rad = -1

            # Draw button body
            pygame.draw.rect(win, self.color, (self.x, self.y,
                             self.width, self.height), 0, border_rad)

            # Draw button border
            if get_var_or_none(self.style_dict, "border-radius"):
                pygame.draw.rect(win, get_var_or_none(self.style_dict, "border-radius"),
                                 (self.x, self.y, self.width, self.height), self.border_width, border_rad)

            # Render text with alignment
            text = self.font.render(self.text, True, get_var_or_none(
                self.style_dict, "text-color"))
            text_align = get_var_or_none(self.style_dict, "text-align")
            text_x, text_y = self.get_text_pos_with_alignment(text, text_align)

            # blit text to screen.
            win.blit(text, (text_x, text_y))

    def move(self, events, mouse):
        # Check if mouse is hovering on button
        mouse_x, mouse_y = mouse.get_pos()

        if mouse_x > self.x and mouse_x < self.x + self.width and mouse_y > self.y and mouse_y < self.y + self.height:
            # Mouse is hovering
            if mouse.get_pressed()[0] and not self.anim_clicking:
                self.anim_clicking = True

                # Set the animation clicking color progress tuple.
                self.anim_clicking_color_prog = tuple(self.color)

                # Check if border-radius exists (otherwise it will be 0)
                # and set animation clicking border radius progress to
                # the number specified.
                if get_var_or_none(self.style_dict, "border-radius"):
                    self.anim_clicking_border_rad_prog = get_var_or_none(
                        self.style_dict, "border-radius")

                # Set other variables
                self.anim_clicking_border_color_prog = tuple(
                    get_var_or_none(self.style_dict, "border-color"))
                self.anim_clicking_text_color_prog = tuple(
                    get_var_or_none(self.style_dict, "text-color"))

                # Text align setting for center, left, and right.
                if ((get_var_or_none(self.style_dict, "text-align-hover") and get_var_or_none(self.style_dict, "text-align-hover") == "center")
                        or get_var_or_none(self.style_dict, "text-align") == "center"):
                    self.anim_clicking_text_align_prog = 0

                if ((get_var_or_none(self.style_dict, "text-align-hover") and get_var_or_none(self.style_dict, "text-align-hover") == "left")
                        or get_var_or_none(self.style_dict, "text-align") == "left"):
                    self.anim_clicking_text_align_prog = -self.width

                else:
                    self.anim_clicking_text_align_prog = self.width
            elif not self.anim_hover:
                self.anim_hover = True

                # Set the animation hover color progress tuple.
                self.anim_hover_color_prog = tuple(self.color)

                # Check if border-radius exists (otherwise it will be 0)
                # and set animation hover border radius progress to
                # the number specified.
                if get_var_or_none(self.style_dict, "border-radius"):
                    self.anim_hover_border_rad_prog = get_var_or_none(
                        self.style_dict, "border-radius")
                self.anim_hover_border_color_prog = tuple(
                    get_var_or_none(self.style_dict, "border-color"))
                self.anim_hover_text_color_prog = tuple(
                    get_var_or_none(self.style_dict, "text-color"))

                # Text align setting for center, left, and right.
                if get_var_or_none(self.style_dict, "text-align") == "center":
                    self.anim_hover_text_align_prog = 0

                if get_var_or_none(self.style_dict, "text-align") == "left":
                    self.anim_hover_text_align_prog = -self.width

                else:
                    self.anim_hover_text_align_prog = self.width

        if self.anim_clicking:
            if get_var_or_none(self.style_dict, "color-clicked-anim") == "linear":
                if not self.anim_clicking_color_prog == get_var_or_none(self.style_dict, "color-clicked"):
                    self.color = self.anim_clicking_color_prog
                    self.click_set1 = False
                self.anim_clicking_color_prog = linearly_set_color(
                    self.anim_clicking_color_prog, get_var_or_none(self.style_dict, "color-clicked"))
            if get_var_or_none(self.style_dict, "border-color-clicked-anim") == "linear":
                if not self.anim_clicking_border_color_prog == get_var_or_none(self.style_dict, "border-color-clicked"):
                    self.click_set2 = False
                self.anim_clicking_border_color_prog = linearly_set_color(
                    self.anim_clicking_border_color_prog, get_var_or_none(self.style_dict, "border-color-clicked"))
            if get_var_or_none(self.style_dict, "border-radius-clicked-anim") == "linear":
                if not self.anim_clicking_border_rad_prog == get_var_or_none(self.style_dict, "border-radius-clicked"):
                    self.click_set3 = False
                self.anim_clicking_border_rad_prog = linearly_set_int(
                    self.anim_clicking_border_rad_prog, get_var_or_none(self.style_dict, "border-radius-clicked"))
            if get_var_or_none(self.style_dict, "text-color-clicked-anim") == "linear":
                if not self.anim_clicking_text_color_prog == get_var_or_none(self.style_dict, "text-color-clicked"):
                    self.click_set4 = False
                self.anim_clicking_text_color_prog = linearly_set_color(
                    self.anim_clicking_text_color_prog, get_var_or_none(self.style_dict, "text-color-clicked"))
            if get_var_or_none(self.style_dict, "text-align-clicked-anim") == "linear":
                if self.anim_clicking_text_align_prog == get_var_or_none(self.style_dict, "text-align-clicked"):
                    self.click_set5 = False
                self.anim_clicking_text_align_prog = linearly_set_int(
                    self.anim_clicking_text_align_prog, conv_alignment_to_num(get_var_or_none(
                        self.style_dict, "text-align-clicked")))
            if not self.click_set1 and not self.click_set2 and not self.click_set3 and not self.click_set4 and not self.click_set5:
                self.anim_clicking = False
                self.click_set1, self.click_set2, self.click_set3, self.click_set4, self.click_set5 = True, True, True, True, True

        if self.anim_hover:
            if get_var_or_none(self.style_dict, "color-hover-anim") == "linear" and self.hover_set1:
                self.color = self.anim_hover_color_prog
                if self.anim_hover_color_prog == get_var_or_none(self.style_dict, "color-hover"):
                    self.hover_set1 = False
                self.anim_hover_color_prog = linearly_set_color(
                    self.anim_hover_color_prog, get_var_or_none(self.style_dict, "color-hover"))
            if get_var_or_none(self.style_dict, "border-color-hover-anim") == "linear" and self.hover_set2:
                if self.anim_hover_border_color_prog == get_var_or_none(self.style_dict, "border-color-hover"):
                    self.hover_set2 = False
                self.anim_hover_border_color_prog = linearly_set_color(
                    self.anim_hover_border_color_prog, get_var_or_none(self.style_dict, "border-color-hover"))
            if get_var_or_none(self.style_dict, "border-radius-hover-anim") == "linear" and self.hover_set3:
                if self.anim_hover_border_rad_prog == get_var_or_none(self.style_dict, "border-radius-hover"):
                    self.hover_set3 = False
                self.anim_hover_border_rad_prog = linearly_set_int(
                    self.anim_hover_border_rad_prog, get_var_or_none("border-radius-hover"))
            if get_var_or_none(self.style_dict, "text-color-hover-anim") == "linear" and self.hover_set4:
                if self.anim_hover_text_color_prog == get_var_or_none(self.style_dict, "text-color-hover"):
                    self.hover_set4 = False
                self.anim_hover_text_color_prog = linearly_set_color(
                    self.anim_hover_text_color_prog, get_var_or_none(self.style_dict, "text-color-hover"))
            if get_var_or_none(self.style_dict, "text-align-hover-anim") == "linear" and self.hover_set5:
                if self.anim_hover_text_align_prog == get_var_or_none(self.style_dict, "text-align-hover"):
                    self.hover_set5 = False
                self.anim_hover_text_align_prog = linearly_set_int(
                    self.anim_hover_text_align_prog, conv_alignment_to_num(get_var_or_none(
                        self.style_dict, "text-align-hover")))
            if not self.hover_set1 and not self.hover_set2 and not self.hover_set3 and not self.hover_set4 and not self.hover_set5:
                self.anim_hover = False
                self.hover_set1, self.hover_set2, self.hover_set3, self.hover_set4, self.hover_set5 = True, True, True, True, True

    def click(self):
        pass

    def get_text_pos_with_alignment(self, text, text_align):
        """This function is for getting the text pos for the button (including alignment)."""

        # Set y position
        text_y = self.y + self.height / 2 - text.get_height() / 2

        # Set x position based on alignment
        if text_align == "left":
            text_x = self.x + get_var_or_none(self.style_dict, "padding")
        if text_align == "right":
            text_x = self.x + self.width - \
                get_var_or_none(self.style_dict, "padding") - \
                text.get_width()
        if text_align == "center":
            text_x = self.x + self.width / 2 - text.get_width() / 2

        return text_x, text_y


minimalistic_dark = {
    "color": [255, 255, 255],
    "color-hover": [0, 0, 0],
    "color-hover-anim": "linear",
    "color-clicked": [50, 50, 50],
    "color-clicked-anim": "linear",
    "border-color": [0, 0, 0],
    "border-color-hover": [255, 255, 255],
    "border-color-hover-anim": "linear",
    "border-color-clicked": [200, 200, 200],
    "border-color-clicked-anim": "linear",
    "border-radius": 20,
    "border-radius-hover": 10,
    "border-radius-hover-anim": "linear",
    "border-radius-clicked": 5,
    "border-radius-clicked-anim": "linear",
    "text-color": [0, 0, 0],
    "text-color-hover": [255, 255, 255],
    "text-color-hover-anim": "linear",
    "text-color-clicked": [200, 200, 200],
    "text-color-clicked-anim": "linear",
    "text-font": "roboto",
    "text-font-hover": "arial",
    "text-font-clicked": "courier",
    "text-align": "center",
    "text-align-hover": "left",
    "text-align-hover-anim": "linear",
    "text-align-clicked": "right",
    "text-align-clicked-anim": "linear",
    "padding": 10
}
